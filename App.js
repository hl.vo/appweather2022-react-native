import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Location from "expo-location";
import { Image } from "react-native";
import { FlatList } from "react-native-web";

const getDate = (dt) => {
  var a = new Date(dt * 1000);
  var months = [
    "Jan",
    "Fev",
    "Mar",
    "Avr",
    "Mai",
    "Juin",
    "Juil",
    "Aout",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var time = date + " " + month + " " + year;
  return time;
};

const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{getDate(title.dt)}</Text>
    <Text>Température : {title.temp.max} °C</Text>
    <Text>Description : {title.weather[0].description}</Text>
    <Image
      style={styles.tinyLogo}
      source={{
        uri:
          "http://openweathermap.org/img/wn/" + title.weather[0].icon + ".png",
      }}
    />
  </View>
);

const renderItem = ({ item }) => <Item title={item} />;

export default function App() {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [cityName, setCityName] = useState("Chargement..");
  const [temp, setTemp] = useState(null);
  const [description, setDescription] = useState(null);
  const [icon, setIcon] = useState(null);
  const [daily, setDaily] = useState(null);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      let locationLatitude = location.coords.latitude;
      let locationLongitude = location.coords.longitude;
      console.log(location);
      console.log(locationLatitude);
      console.log(locationLongitude);

      setLocation(location);

      const axios = require("axios");

      let URLWeatherApi =
        "https://api.openweathermap.org/data/2.5/weather?lat=" +
        locationLatitude +
        "&lon=" +
        locationLongitude +
        "&exclude=daily&appid=cd724356979de34cc1931c707a0281f8&lang=fr&units=metric";

      let URLDailyWeather =
        "https://api.openweathermap.org/data/2.5/onecall?lat=" +
        locationLatitude +
        "&lon=" +
        locationLongitude +
        "&exclude=current,minutely,hourly,alerts&appid=cd724356979de34cc1931c707a0281f8&lang=fr&units=metric";

      axios
        .get(URLWeatherApi)
        .then(function (response) {
          console.log(response);
          setTemp("Température: " + response.data.main.temp + "°C");

          setCityName(response.data.name);

          setDescription(
            "Description: " + response.data.weather[0].description
          );

          setIcon(
            "http://openweathermap.org/img/wn/" +
              response.data.weather[0].icon +
              ".png"
          );
        })
        .catch(function (error) {
          console.log(error);
        });

      axios
        .get(URLDailyWeather)
        .then(function (response) {
          console.log(response);

          setDaily(response.data.daily);
          console.log(response.data.daily);
        })
        .catch(function (error) {
          console.log(error);
        });
    })();
  }, []);

  return (
    <View style={styles.container}>
      <Text>{cityName}</Text>
      <Text>{temp}</Text>
      <Text>{description}</Text>
      <Image style={styles.tinyLogo} source={{ uri: icon }} />

      <Text>Prévision pour les 7 prochains jours</Text>

      <FlatList data={daily} renderItem={renderItem}></FlatList>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  item: {
    backgroundColor: "#f5deb3",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
